# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table persona (
  id                            bigserial not null,
  nombre                        varchar(255),
  profesion                     varchar(255),
  fecha                         timestamptz,
  constraint pk_persona primary key (id)
);


# --- !Downs

drop table if exists persona cascade;

