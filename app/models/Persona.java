package models;

import java.util.*;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.ebean.*;
import play.data.validation.*;

@Entity
public class Persona extends Model{
    @Id
    @GeneratedValue
    @Constraints.Min(10)
    public Long id;

    @Constraints.Required
    public String nombre;

    @Constraints.Required
    public String profesion;

    @Constraints.Required
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    public Date fecha;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public static final Finder<Long, Persona> find = new Finder<>(Persona.class);
}
