package controllers;

import models.Persona;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.*;

import javax.inject.Inject;
import java.util.List;


/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class PersonaController extends Controller {
    @Inject
    private FormFactory formFactory;

    @BodyParser.Of(BodyParser.Json.class)
    public Result create() {
        Form<Persona> personaForm = formFactory.form(Persona.class);
        Persona persona = personaForm.bindFromRequest().get();
        persona.save();
        return  created();
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(String id) {
        Persona personaFound = Persona.find.byId(Long.parseLong(id));
        if(personaFound==null){
            return notFound();
        }else{
            Form<Persona> personaForm = formFactory.form(Persona.class);
            Persona persona = personaForm.bindFromRequest().get();
            personaFound.setNombre(persona.nombre);
            personaFound.setFecha(persona.fecha);
            personaFound.setProfesion(persona.profesion);
            personaFound.update();
            return noContent();
        }
    }

    public Result destroy(String id) {
        Persona persona = Persona.find.byId(Long.parseLong(id));
        if(persona==null){
            return notFound();
        }else{
            persona.delete();
            return ok(Json.toJson(persona));
        }

    }

    public Result show(String id) {
        Persona persona = Persona.find.byId(Long.parseLong(id));
        if(persona==null){
            return notFound();
        }else{
            return ok(Json.toJson(persona));
        }
    }

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        List<Persona> todos = Persona.find.all();
        return ok(Json.toJson(todos));
    }

}
